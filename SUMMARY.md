# Summary

## Global

* [Introduction](README.md)
* [Requirements](requirements.md)
* [Resources](resources.md)
* [Study of alternatives](study-of-alternatives.md)

## Phase 1: Desktop

* [Requirements](version-10/requirements.md)
* [Project Plan](version-10/planification.md)
* [Resources](version-10/resources.md)
* [Technologies to Use](version-10/technologies-used.md)
* [System Architecture](version-10/architecture.md)
* [Workflows](version-10/workflows.md)
* [Mockups](version-10/mockups.md)
* [UI](ui.md)

## Phase 2: Mobile

* [Requirements](v11/requirements.md)
* [Resources](v11/resources-needed.md)
* [Project Plan](v11/planification.md)
* [Technologies to Use](v11/technologies-to-use.md)

## APPENDIX

* [Gitlab Repository](https://gitlab.cern.ch/cern-phone)
* [Markdown Cheatsheet](https://guides.github.com/features/mastering-markdown/)

