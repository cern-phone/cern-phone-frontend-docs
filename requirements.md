# Requirements

## Technical Requirements

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
| 1 | T1 | 1 | Connect to CDS backend | We must use a javascript client to connect to the tone API |
| 1 | T2 | 1 | Web and Desktop application |  |
| 1 | T3 | 1 | Keep user login synchronized between tone backend and the frontend |  |
|  | T4 | 1 | Browser compatibility with WebRTC |  |
|  | T5 |  | Available to start the app on system startup |  |
| 1 | T6 | 1 | Desktop \(Linux, Mac, Windows App\) |  |
|  | T7 | 2 | Mobile \(Android, iOS\) |  |

## Functional Requirements

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
| 1 | F1 | 1 | Login with CERN account |  |
| 1 | F2 | 1 | Logout from CERN account |  |
|  | F3 | 1 | Chat | User will be able to have a chat with other users one-to-one |
|  | F4 | 1 | Ring on incoming call |  |
|  | F5 | 1 | Display messages on incoming call |  |
|  | F6 | 1 | Display notifications on incoming message |  |
|  | F7 | 1 | Allow the user to select microphone |  |
|  | F8 | 1 | Allow the user to select speakers |  |
|  | F9 | 1 | Register the user on tone | Calls and messages will work for him |
|  | F10 | 1 | Unregister the user on tone | The user won't be reachable for calls nor for messages. |
|  | F11 | 1 | Hangup | Hangup a call |
|  | F12 | 1 | Make a call |  |
|  | F13 | 1 | Receive a call |  |
|  | F14 | 1 | Send message |  |
|  | F15 | 1 | Receive message |  |
|  | F16 | 1 | View other user's statuses | Maybe the user is not available for a call, so we may have a way to know about this. On the frontend can be represented as a green/red dot near the user name. |
|  | F17 | 1 | Mute call |  |
|  | F18 | 1 | Search for users | There will be a search field to search for a user to call. Once selected, The user's profile will be displayed and a call can be made from there. |
|  | F19 | 1 | Display user profile | Displays the user information \(name, group, section, if he's available, etc\). We could display other information here in the future. |
|  | F20 |  | Save user on favorites | We can keep a list of favorites for each user. |
|  | F21 |  | Remove user from favorites | Also the users must be removed from favorites. |
|  | F22 |  | Call Forwarding |  |
|  | F23 |  | Conference Calls with at least 3 people |  |
|  | F24 |  | Call transfer |  |
|  | F25 |  | Piquet?? |  |
|  | F26 |  | Team Call?? |  |
|  | F27 |  | Delegates/Supervision |  |
|  | F28 |  | Response Groups |  |
|  | F29 |  | Voice Mailbox |  |
|  | F30 | 1 | Missed Call notifications |  |
|  |  |  |  |  |

## Storage requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  | S1 | Store chat messages on the server??? |  |
|  | S2 | Store user favorites | The user favorites will be stored on the backend. |

## Security

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
|  | SE1 | 1 | User Login with CERN account |  |
|  | SE2 | 1 | Https |  |
|  | SE3 |  | Encode \(obfuscate\) the messages on the server | We won't use a complex algorithm. I think about this on the following premise: we need to do something on the database and we won't be able to see the contents of the user's messages. |
| 1 | SE4 | 1 | JWT to connect the frontend with the backend |  |
| 1 | SE5 | 1 | Oauth to keep the session authenticated between CERN/frontend/backend |  |
| 1 | SE6 | 1 | Encrypted communications |  |
|  | SE7 |  | Support for encrypted communications in IP phones |  |
|  | N/A |  | Central management of IP phones |  |
|  | SE9 |  | Software echo cancellation |  |
|  | SE10 |  | DTMF |  |
|  | N/A |  | Client evolution?? |  |
|  | SE12 |  | Secured architecture |  |
|  | N/A |  | Additional license costs |  |
|  |  |  | Infra Monitoring |  |
|  |  |  | Mac Self Service |  |
|  |  |  | Linux CMF |  |
|  |  |  | User Location |  |
|  |  |  | Contact List |  |
|  |  | 1 | Call History |  |
|  |  |  | Search Corporate Address Book |  |
|  |  |  | ICE/STUN/TURN Support |  |
|  |  |  | Automated bandwidth adjustment |  |
|  |  |  | unified management of personal contacts |  |
|  |  |  | Integration with Outlook |  |
|  |  |  | Integration with Skype |  |
|  |  |  | Integration with other SfB customers |  |
|  |  |  | Call quality indicator |  |
|  |  |  | Video calls |  |
|  |  |  | Content call recording |  |
|  |  |  | Call park |  |
|  |  |  | Multiple calls management |  |
|  |  |  | File sharing |  |
|  |  |  | IPv6 |  |
|  |  |  | Audio Call recording |  |
|  |  |  | Usage statistics |  |
|  |  |  | Remote desktop sharing |  |
|  |  |  | Application sharing |  |
|  |  |  | Whiteboard |  |
|  |  |  | Polls |  |
|  |  |  | Call handover across network types |  |

## User Requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  | U1 | To Have a CERN account |  |
|  | U2 | To Have a phone number? |  |

## UI Requirements

| ID | Name | Description |
| :--- | :--- | :--- |
|  |  |  |



