# Resources Needed

* Need a Mac to develop iOS version \(Xcode needed\).
* iOS and Android phones for development and testing of the
   mobile applications.
* Knowledge about React and React Native
* Knowledge about  Electron
* Knowledge about Python and Django
* Knowledge about PostgreSQL
* It would be great to have several people to develop and test



