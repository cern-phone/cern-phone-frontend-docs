# Study of alternatives

## React Native + Electron

### Desktop

* React + Electron \(Windows, Mac, Linux\)

### Mobile

* React Native \(Android and iOS\)

### Advantages

* Same code on all platforms \(around 85%. Some components are different, like button or navigation menus\).
* Only change the application wrapper \(always react for the code, but electron on desktop and native on mobile\)
* This solution is used by Mattermost.
* Automatic update \(with Electron, not sure about React Native\)
* Better performance than other solutions. The Desktop version will work no matter
   the platform.

### Disadvantages

* Applications are not truly native and won't perfom as well as those \(but they will perform better than webview apps using Cordova or Ionic\).
* 15% of code on the React Native side would be different \(iOS components vs Android ones\) and Electron, and some components must be rewritten, but the overal product quality will be better.

## Other Alternatives

* Ionic: Uses webview \(uses cordova\). Uses angular
* Cordova: Uses webview

One common thing of the above technologies is that they are compatible with Windows and Mac, but Linux is rarely supported. For this reason, I woul go Electron for desktop. There is no silver bullet

## Articles

* A mobile, desktop and website App with the same code:
  [https://medium.com/@benoitvallon/a-mobile-desktop-and-website-app-with-the-same-code-dc84ef7677ee](https://medium.com/@benoitvallon/a-mobile-desktop-and-website-app-with-the-same-code-dc84ef7677ee)
* Five step to learn react:
  [https://www.lullabot.com/articles/how-to-learn-react](https://www.lullabot.com/articles/how-to-learn-react)
* What are the differences between React and React Native:
  [https://medium.com/@alexmngn/from-reactjs-to-react-native-what-are-the-main-differences-between-both-d6e8e88ebf24](https://medium.com/@alexmngn/from-reactjs-to-react-native-what-are-the-main-differences-between-both-d6e8e88ebf24)
* Cordova vs React Native:
  [http://noeticforce.com/mobile-app-development-cordova-vs-react-native-vs-xamarin](http://noeticforce.com/mobile-app-development-cordova-vs-react-native-vs-xamarin)
* How to Choose the best mobile app development framework:
  [https://www.logicroom.co/how-to-choose-the-best-mobile-app-development-framework/](https://www.logicroom.co/how-to-choose-the-best-mobile-app-development-framework/)
* Ionic vs React Native:
  [https://medium.com/@ankushaggarwal/ionic-vs-react-native-3eb62f8943f8](https://medium.com/@ankushaggarwal/ionic-vs-react-native-3eb62f8943f8)
* Ionix vs React Native \(2\):
  [https://www.codementor.io/fmcorz/react-native-vs-ionic-du1087rsw](https://www.codementor.io/fmcorz/react-native-vs-ionic-du1087rsw)
* Ionic vs React Native \(3\):
  [https://hackernoon.com/is-it-possible-to-save-money-and-still-deliver-good-experience-to-a-user-45ea2bae1f83](https://hackernoon.com/is-it-possible-to-save-money-and-still-deliver-good-experience-to-a-user-45ea2bae1f83)
* Ionic vs React Native \(4\):
  [http://www.tothenew.com/blog/building-an-app-ionic-vs-react-native/](http://www.tothenew.com/blog/building-an-app-ionic-vs-react-native/)

## Modules

* Module to use webrtc on react native:
  [https://github.com/oney/react-native-webrtc](https://github.com/oney/react-native-webrtc)
* Module to use SIP on react Native:
  [https://github.com/Carusto/react-native-pjsip](https://github.com/Carusto/react-native-pjsip)
* Authentication to the application backend:
  [https://medium.com/@alexmngn/the-essential-boilerplate-to-authenticate-users-on-your-react-native-app-f7a8e0e04a42](https://medium.com/@alexmngn/the-essential-boilerplate-to-authenticate-users-on-your-react-native-app-f7a8e0e04a42)



