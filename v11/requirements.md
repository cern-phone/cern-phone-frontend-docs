# Requirements

**Deadline**: -

## Technical Requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  | T7 | Mobile App \(Android and iOS\) |  |

## Functional Requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  |  | Same functionalities as phase 1 for mobiles |  |

## Security

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  |  |  |  |

## User Requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  |  |  |  |

## UI Requirements

| ID | Name | Description |
| :--- | :--- | :--- |
|  |  |  |



