# Resources Needed

## Material Resources

* Need a Mac to develop iOS version \(Xcode needed\).
* iOS and Android phones for development and testing of the mobile applications.

## Knowledge

* Knowledge about React and React Native
* Knowledge about Python and Django
* Knowledge about PostgreSQL
* Flexbox
* Openshift and Docker
* Desirable
  * Experience with Android or iOS development

## Workforce

* It would be great to have several people to develop and test \(a core of 2\)
* Students to help



