# Mockups

## Sidebar

![](/assets/Screen Shot 2018-02-14 at 11.46.05.png)

## Login

![](/assets/Screen Shot 2018-02-14 at 11.42.06.png)

### Call: Not on call

![](/assets/Screen Shot 2018-02-14 at 11.47.07.png)

### Call: On call

![](/assets/Screen Shot 2018-02-14 at 11.46.35.png)

### Chat

![](/assets/Screen Shot 2018-02-14 at 11.45.12.png)

## UI Structure

* LoginContainer
* Main Container
  * Sidebar
  * ContentContainer: CallContainer, ChatContainer



