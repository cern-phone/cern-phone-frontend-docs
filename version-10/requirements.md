# Requirements

**Estimated** **Deadline**: September 2018

## Technical Requirements

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
|  | T1 | 1 | Connect to CDS backend | We must use a javascript client to connect to the tone API |
|  | T2 | 1 | Web and Desktop application |  |
|  | T3 | 1 | Keep user login synchronized between tone backend and the frontend |  |
|  | T4 | 1 | Browser compatibility with WebRTC |  |
|  | T6 | 1 | Desktop \(Linux, Mac, Windows App\) |  |

## Functional Requirements

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
|  | F1 | 1 | Login with CERN account |  |
|  | F2 | 1 | Logout from CERN account |  |
|  | F3 | 1 | Chat | User will be able to have a chat with other users one-to-one |
|  | F4 | 1 | Ring on incoming call |  |
|  | F5 | 1 | Display messages on incoming call |  |
|  | F6 | 1 | Display notifications on incoming message |  |
|  | F7 | 1 | Allow the user to select microphone |  |
|  | F8 | 1 | Allow the user to select speakers |  |
|  | F9 | 1 | Register the user on tone | Calls and messages will work for him |
|  | F10 | 1 | Unregister the user on tone | The user won't be reachable for calls nor for messages. |
|  | F11 | 1 | Hangup | Hangup a call |
|  | F12 | 1 | Make a call |  |
|  | F13 | 1 | Receive a call |  |
|  | F14 | 1 | Send message |  |
|  | F15 | 1 | Receive message |  |
|  | F16 | 1 | View other user's statuses | Maybe the user is not available for a call, so we may have a way to know about this. On the frontend can be represented as a green/red dot near the user name. |
|  | F17 | 1 | Mute call |  |
|  | F18 | 1 | Search for users | There will be a search field to search for a user to call. Once selected, The user's profile will be displayed and a call can be made from there. |
|  | F19 | 1 | Display user profile | Displays the user information \(name, group, section, if he's available, etc\). We could display other information here in the future. |
|  | F30 | 1 | Missed Call Notifications |  |
|  | F31 | 1 | I18n |  |

## Storage requirements

None

## Security

| Priority | ID | Phase | Name | Description |
| :--- | :--- | :--- | :--- | :--- |
|  | SE1 | 1 | User Login with CERN account |  |
|  | SE2 | 1 | Https |  |
|  | SE4 | 1 | JWT to connect the frontend with the backend |  |
|  | SE5 | 1 | Oauth to keep the session authenticated between CERN/frontend/backend |  |
|  | SE6 | 1 | Encrypted communications |  |

## User Requirements

| Priority | ID | Name | Description |
| :--- | :--- | :--- | :--- |
|  |  | To Have a CERN account |  |
|  |  | To Have a phone number? |  |

## UI Requirements

| ID | Name | Description |
| :--- | :--- | :--- |
|  |  |  |



