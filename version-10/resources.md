# Resources Needed

## Knowledge

* Knowledge about React, Electron, Webpack
* Knowledge about Python and Django
* Knowledge about PostgreSQL
* CSS, SASS, Flexbox design
* Openshift and Docker

## Workforce

* It would be great to have several people to develop and test \(A core of 2\)
* In summer, we could have another person to help testing the application



