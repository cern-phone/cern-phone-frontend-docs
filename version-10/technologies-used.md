# Technologies to use

* Frontend
  * UI: CSS, SASS, Flexbox, Semantic UI
  * Electron, ReactJS
* Backend: 
  * Python 3.x, Django and Django REST Framework
  * Database: PostgreSQL
  * Openshift and Docker



